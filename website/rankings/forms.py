from django.db import connection
from django import forms
import datetime

class rankingsForm( forms.Form ):

    cursor = connection.cursor()

    genres = (("",""),)
    cursor.execute("SELECT * FROM Genres")
    genres = genres + cursor.fetchall()

    countries = (("",""),)
    cursor.execute("""SELECT 1,'USA' UNION SELECT * FROM Countries WHERE Countries.Name != 'USA'""")
    countries = countries + cursor.fetchall()

    actors = (("",""),)
    cursor.execute("""SELECT * FROM Actors""")
    actors = actors + cursor.fetchall()

    years = (("",""),)
    for num in reversed(range(1903,2012)):
        years = years + ((num,num),)

    Genre = forms.ChoiceField(choices=genres, required=False)
    Country = forms.ChoiceField(choices=countries, required=False)
    Year = forms.ChoiceField(choices=years, required=False)
    Actor = forms.CharField(required=False)
    Director = forms.CharField(required=False)
