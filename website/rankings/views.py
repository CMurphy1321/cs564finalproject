from django.shortcuts import render
from django.db import connection
from .forms import rankingsForm
from helpers import LIKE

def rankings(request):
    cursor = connection.cursor()
    header = []
    params = ()

    # Building query
    ###################################

    SELECT = "SELECT Movies.Title, Movies.Year, F_SuperSecretScores.Score"
    FROM = " FROM Movies INNER JOIN F_SuperSecretScores ON Movies.movieID = F_SuperSecretScores.movieID"
    WHERE = ""
    LIMIT = ""
    ORDERBY = " ORDER BY Score DESC"

    country = str(request.POST.get('Country'))
    genre = str(request.POST.get('Genre'))
    actor = str(request.POST.get('Actor'))
    director = str(request.POST.get('Director'))
    year = str(request.POST.get('Year'))

    if( allNone([genre,country,actor,director,year]) ):
        LIMIT = " LIMIT 0"

    else:
        # WHERE = " WHERE"

        # genre, country, actors, year, director
        # Genre slicer
        if( not isNone(genre) ):
            cursor.execute("""SELECT Genre FROM Genres WHERE genreID=%s""" % (genre) )
            genre = cursor.fetchall()
            genre = genre[0]
            genre = str(genre).split("'")[1]
            FROM = FROM + " INNER JOIN (SELECT * FROM F_GenresOneLineTable WHERE F_GenresOneLineTable.genres LIKE %s) GenresOneLine ON Movies.movieId=GenresOneLine.movieID"
            params = params + (LIKE(genre),)

        # Country slicer
        if( not isNone(country) ):
            params = params + (country,)
            FROM = FROM + " INNER JOIN (SELECT * FROM Countries WHERE Countries.countryID=%s) Country ON Movies.countryID = Country.countryID"

        # Actor slicer
        if( not isNone(actor) ):
            params = params + (LIKE(actor),)
            FROM = FROM + " INNER JOIN (SELECT DISTINCT Acts_In.movieID FROM (SELECT * FROM Actors WHERE Actors.Name LIKE %s) a1 inner join Acts_In on a1.actorID=Acts_In.actorID) a2 on a2.movieID=Movies.movieID"
 
        # Director slicer
        if( not isNone(director) ):
            params = params + (LIKE(director),)
            FROM = FROM + " INNER JOIN (SELECT * FROM Directors WHERE Directors.Name LIKE %s) Director ON Movies.directorID=Director.directorID"

        # Year slicer
        if( not isNone(year) ):
            params = params + (year,)
            WHERE = " WHERE Movies.Year=%s"

    query = SELECT + FROM + WHERE + ORDERBY + LIMIT
    print("#########################")
    print(query)
    print("#########################")

    ###################################
    cursor.execute(query % (params))

    data = cursor.fetchall()

    for column in cursor.description:
        header.append(column[0])

    records = cursor.rowcount

    form = rankingsForm(request.POST or None)
    context = {
        "form": form,
        "header": header,
        "data": data,
        "records": records,
    }
    return render(request,'rankings/rankings.html',context)

def allNone(ary):
    for elem in ary:
        if( (elem != "None") & (elem != "") ):
            return False
    return True

def isNone( val ):
    val = str(val)
    if( (val != "None") & (val != "") ):
        return False
    return True
