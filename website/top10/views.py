from django.shortcuts import render
from django.db import connection

def top10(request):
    cursor = connection.cursor()
    header = []

    cursor.execute("""Select Title,Year,Director,Genres,Country,Score From F_Top10 Order by score desc""")
    data = cursor.fetchall()

    for column in cursor.description:
        header.append(column[0])

    context = {
        "header": header,
        "data": data
    }
    return render(request,'top10/top10.html',context)
