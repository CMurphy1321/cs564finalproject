from django.shortcuts import render
from django.db import connection
from .forms import searchForm
from helpers import LIKE

def search(request):
    cursor = connection.cursor()
    header = []

    query = str(request.POST.get('Search'))

    if( query != "None" ):
        cursor.execute("""SELECT Movies.Title,Directors.name Director,Countries.Name Country,Movies.Year, F_GenresOneLineTable.Genres, F_SuperSecretScores.Score FROM Movies LEFT OUTER JOIN Directors ON Movies.directorID=Directors.directorID LEFT OUTER JOIN Countries ON Movies.countryID = Countries.countryID INNER JOIN F_GenresOneLineTable ON Movies.movieID = F_GenresOneLineTable.movieID INNER JOIN F_SuperSecretScores ON Movies.movieID = F_SuperSecretScores.movieID WHERE Movies.Title like %s""" % (LIKE(query)))
    else:
        cursor.execute("""SELECT Movies.Title,Directors.name Director,Countries.Name Country,Movies.Year, F_GenresOneLineTable.Genres, F_SuperSecretScores.Score FROM Movies LEFT OUTER JOIN Directors ON Movies.directorID=Directors.directorID LEFT OUTER JOIN Countries ON Movies.countryID = Countries.countryID INNER JOIN F_GenresOneLineTable ON Movies.movieID = F_GenresOneLineTable.movieID INNER JOIN F_SuperSecretScores ON Movies.movieID = F_SuperSecretScores.movieID WHERE Movies.Title like %s""" % ("''"))
    data = cursor.fetchall()
    records = cursor.rowcount

    for column in cursor.description:
        header.append(column[0])

    form = searchForm(request.POST or None)
    context = {
        "form": form,
        "header": header,
        "data": data,
        "records": records,
    }
    return render(request,'search/search.html',context)
