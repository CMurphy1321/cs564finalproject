sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get install libmysqlclient-dev
sudo apt-get install mysql-server-5.6
sudo apt-get install python-pip
sudo apt-get install ipython
sudo apt-get install -y sublime-text-installer
sudo pip2 install django
sudo pip2 install --upgrade django-debug-toolbar
sudo pip2 install --upgrade django-crispy-forms
sudo pip2 install MySQL-python
