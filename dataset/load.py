import MySQLdb
import time

def loadDB():
    totalStart = time.time()
    db = MySQLdb.connect(host="localhost", user="root", db="cs564finalproject")
    cursor = db.cursor()

    perfLoad('loadMovies(cursor,db)',cursor,db)
    perfLoad('loadDirectors(cursor,db)',cursor,db)
    perfLoad('loadGenres(cursor,db)',cursor,db)
    perfLoad('loadRotten(cursor,db)',cursor,db)
    perfLoad('loadMovieLensRatings(cursor,db)',cursor,db)
    perfLoad('loadMovieGenres(cursor,db)',cursor,db)
    perfLoad('loadMovieLensRatings(cursor,db)',cursor,db)
    perfLoad('loadCountries(cursor,db)',cursor,db)
    perfLoad('loadActors(cursor,db)',cursor,db)
    perfLoad('loadActsIn(cursor,db)',cursor,db)
    perfLoad('updateDirectors(cursor,db)',cursor,db)
    perfLoad('updateCountries(cursor,db)',cursor,db)
    perfLoad('loadStoredProcedures(cursor,db)',cursor,db)

    db.close()

    print("Overall took: " + str(time.time() - totalStart))

def perfLoad(func,cursor,db):
    start = time.time()
    exec(func)
    print( str(func.split("(")[0]) + " took " + str( time.time() - start ) )

def loadMovies(cursor, db):
    print("Dropping table Movies and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS Movies")
        cursor.execute("CREATE TABLE Movies(movieID Integer,Title Varchar(125), directorID Integer, Year Integer, countryID Integer)")
        db.commit()
    except:
        db.rollback()

    with open('movies.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            movieID = int(data[0])
            title = str(data[1])
            year = int(data[5])

            try:
                cursor.execute("""INSERT INTO Movies (movieID,Title,Year) VALUES(%s,%s,%s)""",(movieID,title,year))
                db.commit()
            except:
                db.rollback()
                print("Couldn't insert " + str(movieID))

def loadGenres(cursor, db):
    print("Loading Genres...")

    try:
        cursor.execute("DROP TABLE IF EXISTS Genres")
        cursor.execute("CREATE TABLE Genres(genreID Integer AUTO_INCREMENT PRIMARY KEY, Genre VarChar(50), UNIQUE(Genre))")
        db.commit()
    except:
        db.rollback()

    with open('movie_genres.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            name = str(data[1])

            try:
                cursor.execute("""INSERT INTO Genres (Genre) VALUES('%s')""" % (name))
                db.commit()
            except:
                db.rollback()

def loadDirectors(cursor, db):
    print("Loading Directors...")

    try:
        cursor.execute("DROP TABLE IF EXISTS Directors")
        cursor.execute("CREATE TABLE Directors(directorID Integer AUTO_INCREMENT PRIMARY KEY, Name VarChar(50), UNIQUE(Name))")
        db.commit()
    except:
        db.rollback()

    with open('movie_directors.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            name = str(data[2]).replace("'","''")

            try:
                cursor.execute("""INSERT INTO Directors (Name) VALUES('%s')""" % (name))
                db.commit()
            except:
                db.rollback()

def updateDirectors(cursor, db):
    print("Updating Directors...")
    with open('movie_directors.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            movieID = data[0]
            director = data[2].replace("'","''")

            try:
                cursor.execute("""UPDATE Movies SET directorID=(SELECT directorID FROM Directors WHERE name=%s) WHERE movieID=%s""",(director,movieID))
                db.commit()
            except:
                db.rollback()
                print("Couldn't insert " + str(movieID) + director)

def loadCountries(cursor, db):
    print("Dropping table Countries and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS Countries")
        cursor.execute("CREATE TABLE Countries(countryID Integer AUTO_INCREMENT PRIMARY KEY, Name VARCHAR(30), UNIQUE(Name))")
        db.commit()
    except:
        db.rollback()

    with open('movie_countries.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")

            try:
                country = data[1]
                cursor.execute("""INSERT INTO Countries (Name) VALUES('%s')""" % (country))
                db.commit()
            except:
                db.rollback()

def updateCountries(cursor, db):
    print("Updating Countries...")
    with open('movie_countries.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            movieID = data[0]
            # Don't quit if movie is not associated with a country
            try:
                cursor.execute("""SELECT countryID from Countries WHERE Name = '""" + data[1] + "'")
                country = cursor.fetchall()
                country = country[0]
                country = str(country).split("L")[0]
                country = str(country).split("(")[1]
                cursor.execute("""UPDATE Movies SET countryID=%s WHERE movieID=%s""",(country,movieID))
                db.commit()
            except:
                db.rollback()
                print("Couldn't insert " + str(movieID) + country)

def loadMovieLensRatings(cursor, db):
    print("Dropping table MovieLens_Rating and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS MovieLens_Rating ")
        cursor.execute("CREATE TABLE MovieLens_Rating ( movieID Integer, userID Integer, Rating Float )")
        db.commit()
    except:
        db.rollback()

    with open('user_ratedmovies.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            userID = int(data[0])
            movieID = int(data[1])
            rating = float(data[2])

            try:
                cursor.execute("""INSERT INTO MovieLens_Rating (movieID, userID, Rating) VALUES(%s,%s,%s)""",(movieID,userID,rating))
                db.commit()
            except:
                db.rollback()
                print("Couldn't insert " + str( movieID ))

def loadMovieGenres(cursor, db):
    print("Dropping table Genres and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS Movie_Genres")
        cursor.execute("CREATE TABLE Movie_Genres(movieID Integer, genreID Integer)")
        db.commit()
    except:
        db.rollback()

    with open('movie_genres.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            movieID = int(data[0])
            cursor.execute("""SELECT genreID from Genres WHERE Genre = '""" + data[1] + "'")
            genre = cursor.fetchall()
            genre = genre[0]
            genre = str(genre).split("L")[0]
            genre = str(genre).split("(")[1]

            try:
                cursor.execute("""INSERT INTO Movie_Genres (movieID,genreID) VALUES(%s,%s)""",(movieID,genre))
                db.commit()
            except:
                db.rollback()
                print("Couldn't insert " + str(movieID) + ": " + genre)

def loadActors(cursor, db):
    print("Dropping table Actors and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS Actors")
        cursor.execute("CREATE TABLE Actors(actorID Varchar(50), name Varchar(50), PRIMARY KEY(actorID))")
        db.commit()
    except:
        print("Couldn't make table Actors.")
        db.rollback()
        return

    with open('movie_actors.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            actorID = str(data[1])
            name = str(data[2])

            try:
                cursor.execute("""INSERT INTO Actors (actorID,name) VALUES(%s,%s)""",(actorID,name))
            except:
                db.rollback()
                # print("Couldn't insert " + str(actorID) + ": " + name)
            db.commit()

def loadActsIn(cursor, db):
    print("Dropping table Acts_In and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS Acts_In")
        cursor.execute("CREATE TABLE Acts_In(movieID Integer, actorID Varchar(50), PRIMARY KEY(movieID, actorID))")
    except:
        db.rollback()

    with open('movie_actors.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            movieID = int(data[0])
            actorID = str(data[1])

            try:
                cursor.execute("""INSERT INTO Acts_In (movieID, actorID) VALUES(%s,%s)""",(movieID,actorID))
                db.commit()
            except:
                db.rollback()
                print("Couldn't insert " + str(actorID) + ": " + str(movieID))

def loadRotten(cursor, db):
    print("Dropping table Rotten_Tomato_Ratings and inserting into it...")
    try:
        cursor.execute("DROP TABLE IF EXISTS Rotten_Tomato_Ratings")
        cursor.execute("CREATE TABLE Rotten_Tomato_Ratings(movieID Integer, all_Crit_Rating Float, all_Crit_Num_Fresh Integer, all_Crit_Num_Rotten Integer, top_Crit_Rating Float, top_Crit_Num_Fresh Integer, top_Crit_Num_Rotten Integer, top_Crit_Score Integer, audience_Rating Float, audience_Score Integer, num_Audience_Rating float)")
        # Rotten_Tomato_Ratings (movieID, all_Crit_Rating, all_Crit_Num_Fresh, all_Crit_Num_Rotten, top_Crit_Rating, top_Crit_Num_Fresh, top_Crit_Num_Rotten, top_Crit_Score, audience_Rating, audience_Score, num_Audience_Rating)

        db.commit()
    except:
        db.rollback()

    with open('movies.dat') as file:
        file.readline()

        for line in file:
            line = line.rstrip()
            data = line.split("\t")
            movieID = int(data[0])
            if( data[7] != '\N'):
                all_Crit_Rating = data[7]
                all_Crit_Num_Fresh = data[8]
                all_Crit_Num_Rotten = data[9]
                top_Crit_Rating = data[12]
                top_Crit_Num_Fresh = data[14]
                top_Crit_Num_Rotten = data[15]
                top_Crit_Score = data[16]
                audience_Rating = data[17]
                audience_Score = data[19]
                num_Audience_Rating = data[18]

                try:
                    cursor.execute("""INSERT INTO Rotten_Tomato_Ratings (movieID, all_Crit_Rating, all_Crit_Num_Fresh, all_Crit_Num_Rotten, top_Crit_Rating, top_Crit_Num_Fresh, top_Crit_Num_Rotten, top_Crit_Score, audience_Rating, audience_Score, num_Audience_Rating) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(movieID,all_Crit_Rating,all_Crit_Num_Fresh,all_Crit_Num_Rotten,top_Crit_Rating,top_Crit_Num_Fresh,top_Crit_Num_Rotten,top_Crit_Score,audience_Rating,audience_Score,num_Audience_Rating))
                    db.commit()
                except:
                    db.rollback()
                    print("Couldn't insert " + str(movieID))

# def loadStoredProcedures(cursor,db):
    #storedProc = "create procedure BuildScores() begin drop table if exists F_SuperSecretScores; create table F_SuperSecretScores(movieID integer, Score float); insert into F_SuperSecretScores (movieID,score) select m.movieID, (0.5*(2*all_Crit_Rating + 2*top_Crit_Rating + 0.1*top_Crit_Score + 6*audience_Rating + 0.2*audience_Score + num_Audience_Rating/100000) + 10*(select avg(ml1.Rating) from MovieLens_Rating ml1 where ml1.movieID=m.movieID)) as score from Movies m Left outer join Rotten_Tomato_Ratings rt on m.movieID = rt.movieID Order by score desc; end"
    # delimiter //
    # drop procedure if exists BuildScores;
    # create procedure BuildScores()
    # begin
    #     drop table if exists F_SuperSecretScores;
    #     create table F_SuperSecretScores(movieID integer, Score float);

    #     insert into F_SuperSecretScores (movieID,score)
    #     select rt.movieID,
    #     (0.5*(2*all_Crit_Rating + 2*top_Crit_Rating + 0.1*top_Crit_Score
    #      + 6*audience_Rating + 0.2*audience_Score + num_Audience_Rating/100000)
    #      + (10*(select avg(ml.Rating) from MovieLens_Rating ml where ml.movieID=rt.movieID))) as score
    #     from Rotten_Tomato_Ratings rt
    #     where rt.movieID in (select distinct movieID from MovieLens_Rating)
    #     Order by score desc;

    # end //
    # delimiter ;

    # DELIMITER //
    # DROP TABLE IF EXISTS F_GenresOneLineTable;
    # CREATE TABLE F_GenresOneLineTable(movieID int, genres varchar(200));
    # DROP PROCEDURE IF EXISTS combineGenres;
    # CREATE PROCEDURE combineGenres()
    # BEGIN  

    # DECLARE done INT DEFAULT 0;  
    # DECLARE id1, id2 INT;
    # DECLARE genre VARCHAR(32);
    # DECLARE genreOneLine VARCHAR(200);
    # DECLARE count INT;
    # DECLARE last INT;

    # DECLARE cur CURSOR FOR SELECT m.movieID,g.Genre FROM Movies m 
    #   LEFT OUTER JOIN Movie_Genres mg on m.movieID = mg.movieID
    #   LEFT OUTER JOIN Genres g ON mg.genreID = g.genreID 
    #   ORDER BY m.movieID,g.Genre DESC;  
    # DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    # OPEN cur;  

    # SET count := 0;
    # set last := 1;
    # SET genreOneLine := ""; 

    
    #     Loop2: LOOP  
            
    #             FETCH cur INTO id2,genre;   
    #         IF done THEN    
    #           INSERT INTO F_GenresOneLineTable(movieID,genres) VALUES (last,genreOneLine);
    #           LEAVE loop2;
    #         End if;
            
    #         SET count := count + 1;

    #         IF last<>id2 THEN 
    #           INSERT INTO F_GenresOneLineTable(movieID,genres) VALUES (last,genreOneLine);
    #           set last := id2;
    #           SET genreOneLine := "";
    #           SET count := 1;
    #         END if;
    #         IF count = 1 THEN
    #           SET genreOneline := genre;
    #         ELSE
    #           SET genreOneline := CONCAT(genreOneLine,', ',genre);
    #         END if;
    #     END LOOP Loop2;
           
    # CLOSE cur;
    # END //
    # DELIMITER ;

    # delimiter //
    # drop table if exists F_Top10;
    # create table F_Top10(title varchar(125),year int,director varchar(50),genres varchar(200),country varchar(30),score float);
    # drop procedure if exists Top10;
    # create procedure Top10()
    # begin

    #     insert into F_Top10 (title,year,director,genres,country,score)
    #     select m.Title,m.Year,d.Name,g.genres,c.Country,s.score
    #     from Movies m
    #     inner join F_SuperSecretScores s on m.movieID = s.movieID
    #     inner join F_GenresOneLineTable g on m.movieID = g.movieID
    #     left outer join Directors d on m.directorID = d.directorID
    #     left outer join Countries c on m.countryID = c.countryID
    #     where s.score is not null
    #     Order by s.score desc
    #     limit 10;

    # end //
    # delimiter ;

loadDB()
